/*

    Activity(session24 database)

    >> Find users with letter 'y' in their firstName OR lastName
        >> show only their email and isAdmin properties/fields

    >> Find users with letter 'e' in their firstName AND is an admin.
        >> show only their email and isAdmin properties/fields

    >> Find products with letter x in its name AND has a price greater than or equal to 50000

    >> Update all products with price less than 2000 to inactive.

    >> Delete all products with price greater than 20000.


    //Add all of your query/commands here in activity.js
*/

db.users.find(
    {
    $or: [
        {"firstName" : {$regex: 'y', $options: '$i'}},
        {"lastName" : {$regex: 'y', $options: '$i'}}
    ]
    },
        {"_id" : 0, "email" : 1, "isAdmin" : 1}
    )

db.users.find({
    $and: 
    [
        {"firstName" : {$regex: 'e', $options: '$i'}},
        {"isAdmin" : true}
    ]
    },
        {"_id" : 0, "email" : 1, "isAdmin" : 1})

db.products.find({
    $and: [
        {"name" : {$regex: 'x', $options: '$i'}},
        {"price" : {$gt: 50000}}
    ]})

db.products.updateMany({}, {$set: {"isActive" : true}})

db.products.deleteMany({"price" : {$gt: 20000}})